import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

//el index llama a root que es donde se guarda(el contenido de app) y asi se despliega

const root = ReactDOM.createRoot(document.getElementById('root'));//seleccione el elemento 'root'
root.render(//y renderice(cargue) un componenete llamado App
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
