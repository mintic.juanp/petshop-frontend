import React, {Fragment} from 'react';//importamos React y el componente llamado fragment (define una zona o frangmento de la pagina)
import{BrowserRouter as Router, Routes, Route} from 'react-router-dom'; //importe {} desde la extencion 'react-router-dom'
/* 
*el componente BrowserRouter con el aliaz Router, es para gestion de rutas
*Routes, sirve para importar o decir varias rutas 
*Route, sirve para  decir una ruta particular
*/
import Login from './pages/auth/Login';//importeme la fincion login
import Register from './pages/auth/Register';
import Home from './pages/auth/Home';
import Profile from './pages/auth/profile';
import Productos from './pages/Productos/Productos';
import Perro from './pages/Animales/Perro';
import Gato from './pages/Animales/Gato';
import Alimentos from './pages/Categorias/Alimentos';
import Limpieza from './pages/Categorias/Limpieza';
import Accesorios from './pages/Categorias/Accesorios';
import ProdAdm from './pages/Productos/ProductosAdm';
import AgregaProd from './pages/Productos/AgregarProd';
import EditProd from './pages/Productos/EditarProd';
import ElimProd from './pages/Productos/EliminarProd';
import AgregarPedido from './pages/Pedidos/AgregPedido';

//La funcion app se encarga de cargar cuando la pagina abre
function App() {
  return (
      //<Fragment> En esta Zona o frangemneto cargueme....
        //<Router> ...Cargueme rutas
          //<Routes> ...Cargue una lista de rutas
            //<Route> ...Cargue una ruta Especifica

      <Fragment> 
        <Router>
          <Routes>
            <Route path='/' exact element= {<Home />} />
            <Route path='/login' exact element= {<Login />} /> {/* Con la ruta '/' abrame el elemento <Login /> */}
            <Route path='/register' exact element= {<Register/>} /> {/* Con la ruta '/register' abrame el elemento <Register /> */}
            <Route path='/profile' exact element= {<Profile/>} />
            <Route path='/prodAdm' exact element= {<ProdAdm/>} />
            <Route path='/productos' exact element= {<Productos/>} />
            <Route path='/perro' exact element= {<Perro/>} />
            <Route path='/gato' exact element= {<Gato/>} />
            <Route path='/alimentos' exact element= {<Alimentos/>} />
            <Route path='/limpieza' exact element= {<Limpieza/>} />
            <Route path='/accesorios' exact element= {<Accesorios/>} />
            <Route path='/agregPedido/:id' exact element= {<AgregarPedido/>} />
            <Route path='/agregProd' exact element= {<AgregaProd/>} />
            <Route path='/editProd/:id' exact element= {<EditProd/>} />
            <Route path='/elimProd' exact element= {<ElimProd/>} />
          </Routes>
        </Router>
      </Fragment>
  );
}

export default App;
