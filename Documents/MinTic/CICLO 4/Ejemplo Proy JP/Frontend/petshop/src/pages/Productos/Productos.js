import React, { useEffect, useState } from "react";//importe la extencion React
import { Link } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import APIInvoke from "../../utils/APIInvoke";

//Para crear la funcion flecha rapido ponemos "sfc" y enter
const Productos = () => {

    const [prod, setProd] = useState([]);//Saqueme la lista de elementos [] y guardemela en prod

    const [search, setSearch ] = useState("");

    const loadProd = async () => {

        const response = await APIInvoke.invokeGET('/producto/list');//guarde la respuesta de esperar el APIInvoke.invokeGET('')

        setProd(response.result)
        //console.log(response)
    };

    const searchEvent = async (e)=>{

        e.preventDefault();

        const response = await APIInvoke.invokeGET(`/producto/list/`+search);
        
        //alert(JSON.stringify(response.message))
        
        setProd(response.message)
        
        //console.log(response)

    };

    const onChange= (event) => {
        setSearch(event.target.value);   
    }

    useEffect(() => {

        loadProd();

    }, []) //45.57

    //Pegamos el Codigo de la ventana de Home de bootstrap 4
    //nos da Error pues hay palabras reservadas en js que se usan en Html, entonces lo convertimos
    //usamos la extencion html to jxs, seleccionamos el codigo dentro del <div>...</div> y le damos clack y le ponemos convert Html to JXS
    //Para Ordenarlo Seleccionamos el codigo, le damos clack y "Dar formato al documento con..." 
    return (

        <div>
            <div>
                {/* imagen */}
                <div align="center">{/* Centrar Imagen y poner imagen */}
                    <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                        height={300} />
                </div>
                {/* Nombre*/}
                <div className="text-center">
                    <div className="h1"><b>CatDog</b>Store</div>
                    <br />
                    <br />
                </div>

                {/* Barra Busqueda*/}
                <div className="row justify-content-center">
                    <div className="col-12 col-md-10 col-lg-8">
                        <form className="card card-sm" onSubmit={searchEvent}>
                            <div className="card-body row no-gutters align-items-center">
                                {/*end of col*/}
                                <div className="col">
                                    <input className="form-control form-control-lg form-control-borderless" 
                                    type="text" 
                                    id="search"
                                    name="search"
                                    value={search}
                                    onChange = {onChange}
                                    placeholder="Dejanos Ayudarte...¿Que Buscas?" />
                                </div>
                                {/*end of col*/}
                                <div className="col-auto">
                                    <button className="btn btn-lg btn-success" type="submit" >Buscar</button>
                                </div>
                                {/*end of col*/}
                            </div>
                        </form>
                    </div>
                    {/*end of col*/}
                </div>
                <br />
                <br />


                {/* Barra*/}
                <div>
                    <ul className="nav nav-tabs navbar-light nav-fill">
                        <li className="nav-item">
                            <Link to={"/"} className="nav-link" href="#">Inicio</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/productos"} className="nav-link active" href="#">Productos</Link>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Animal</a>
                            <div className="dropdown-menu">
                                <Link to={"/perro"} className="dropdown-item" href="#">Perro</Link>
                                <Link to={"/gato"} className="dropdown-item" href="#">Gato</Link>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Categorias</a>
                            <div className="dropdown-menu">
                                <Link to={"/alimentos"} className="dropdown-item" href="#">Alimentos</Link>
                                <Link to={"/limpieza"} className="dropdown-item" href="#">Limpieza</Link>
                                <Link to={"/accesorios"} className="dropdown-item" href="#">Accesorios</Link>
                            </div>
                        </li>
                        <li className="nav-item">
                            <Link to={"/login"} className="btn btn-outline-danger" href="#">Login</Link>
                        </li>
                    </ul>
                </div>
                {/* Carrusel 1 */}

                <div className="jumbotron jumbotron-fluid">
                    <div className="container justify-center">
                        <div id="carouselExampleSlidesOnly" className="carousel slide" data-ride="carousel" data-interval="2000">
                            <div className="carousel-inner">
                                <div className="carousel-item active text-center">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiXdZQHsY4IQG924O07-WqdKgG5KIj7qOC-q3rWgmgUrn0ISvciG5zkN-1usbp9OYvpbE&usqp=CAU" height={150} />
                                </div>
                                <div className="carousel-item text-center">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS45wL_ZdVFBXKaczANt5eq7NofCoVLxdHR6Q&usqp=CAU" height={150} />
                                </div>
                                <div className="carousel-item text-center">
                                    <img src="https://www.purina-latam.com/sites/g/files/auxxlc391/files/2022-05/purina-registro-caring.jpg" height={150} />
                                </div>
                                <div className="carousel-item text-center">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRCI3i-JMs0G9oTdegn2B9UGg7EcT-dLgHapKM4sbPwkSgtE9U2dzdom646qVeyUhauA&usqp=CAU" height={150} />
                                </div>
                                <div className="carousel-item text-center">
                                    <img src="https://productosparamascotas.cl/modules/ps_banner/img/58e79457dd2ba78f8a7de84f17a6fb93.png" height={150} />
                                </div>
                                <div className="carousel-item text-center">
                                    <img src="https://www.kiwoko.com/blogmundoanimal/wp-content/uploads/2022/09/es_bcat_primera_compra.jpg" height={150} />
                                </div>
                                <div className="carousel-item text-center">
                                    <img src="https://distribuidorasanjose.cl/wp-content/uploads/2022/03/BANNER.jpg" height={150} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                {/*Cards*/}
                <div className="container">
                    <div className="card-columns">
                            {
                                prod.map(
                                    item =>
                                    <div className="card border-primary" key={item._id}>
                                        <div className="card border-primary">
                                            <img src={item.urlImage} className="card-img-top" alt="..." />
                                            <div className="card-body">
                                                <p className="card-text">{item.descripcion}</p>
                                                <ul className="list-group list-group-flush">
                                                    <li className="list-group-item">{item.categoria}</li>
                                                    <li className="list-group-item">{item.animal}</li>
                                                    <li className="list-group-item h3">{"$ "+item.precio}</li>
                                                </ul>
                                                <Link to={`/agregPedido/${item._id}`} href="#" className="btn btn-primary">Comprar</Link>
                                            </div>
                                        </div>
                                    </div>
                            )}
                    </div>
                </div>

            </div>
        </div>

    );
}

export default Productos;

/*
CODIGO TARJETAS

<div className="container">
                    <div className="row row-cols-1 row-cols-sm-3 g-4">

                        <div className="col">
                            <div className="card h-100">
                                <img src="https://www.agrocampo.com.co/media/catalog/product/cache/d51e0dc10c379a6229d70d752fc46d83/1/1/111100319_ed-min.jpg" className="card-img-top" alt="..." />
                                <div className="card-body">
                                    <p className="card-text">Max Professional Line Perros Adultos Light Pollo y Arroz 15 Kg</p>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Comida</li>
                                        <li className="list-group-item">Perro</li>
                                        <li className="list-group-item h3">$ 246.605</li>
                                    </ul>
                                    <a href="#" className="btn btn-primary">Comprar</a>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card h-100">
                                <img src="https://www.agrocampo.com.co/media/catalog/product/cache/d51e0dc10c379a6229d70d752fc46d83/1/1/111100319_ed-min.jpg" className="card-img-top" alt="..." />
                                <div className="card-body">
                                    <p className="card-text">Max Professional Line Perros Adultos Light Pollo y Arroz 15 Kg</p>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Comida</li>
                                        <li className="list-group-item">Perro</li>
                                        <li className="list-group-item h3">$ 246.605</li>
                                    </ul>
                                    <a href="#" className="btn btn-primary">Comprar</a>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card h-100">
                                <img src="https://www.agrocampo.com.co/media/catalog/product/cache/d51e0dc10c379a6229d70d752fc46d83/_/3/_3_1_313160025_ed-min.jpg" className="card-img-top" alt="..." />
                                <div className="card-body">
                                    <p className="card-text">Cat can PopoCatch Perro y gato 300 ml</p>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Limpieza</li>
                                        <li className="list-group-item">Perro/Gato</li>
                                        <li className="list-group-item h3">$ 22.695</li>
                                    </ul>
                                    <a href="#" className="btn btn-primary">Comprar</a>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card h-100">
                                <img src="https://www.agrocampo.com.co/media/catalog/product/cache/d51e0dc10c379a6229d70d752fc46d83/3/2/323213313-min.jpg" className="card-img-top" alt="..." />
                                <div className="card-body">
                                    <p className="card-text">Animal Factor Trailla Pechera Maya color Azul Talla M 0-465-2</p>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Accesorios</li>
                                        <li className="list-group-item">Perro</li>
                                        <li className="list-group-item h3">$ 27.120</li>
                                    </ul>
                                    <a href="#" className="btn btn-primary">Comprar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

*/

/*
CODIGO MAPEO

<div className="container">
                    <div className="row row-cols-1 row-cols-sm-3 g-4">
                        <div className="col">

                            {
                                prod.map(
                                    item =>
                                        <div className="card h-100">
                                            <img src={item.urlImage} className="card-img-top" alt="..." />
                                            <div className="card-body">
                                                <p className="card-text">{item.descripcion}</p>
                                                <ul className="list-group list-group-flush">
                                                    <li className="list-group-item">{item.categoria}</li>
                                                    <li className="list-group-item">{item.animal}</li>
                                                    <li className="list-group-item h3">{item.precio}</li>
                                                </ul>
                                                <a href="#" className="btn btn-primary">Comprar</a>
                                            </div>
                                        </div>

                                )};
                        </div>
                    </div>
                </div>
*/