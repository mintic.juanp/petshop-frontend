
import { Link } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import React, { useEffect, useState } from "react";//importe la extencion React
import APIInvoke from "../../utils/APIInvoke";

//Para crear la funcion flecha rapido ponemos "sfc" y enter
const Home = () => {

    //Pegamos el Codigo de la ventana de Home de bootstrap 4
    //nos da Error pues hay palabras reservadas en js que se usan en Html, entonces lo convertimos
    //usamos la extencion html to jxs, seleccionamos el codigo dentro del <div>...</div> y le damos clack y le ponemos convert Html to JXS
    //Para Ordenarlo Seleccionamos el codigo, le damos clack y "Dar formato al documento con..." 
    /*
      <style>
      .contenedor{
          background-image: url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiXdZQHsY4IQG924O07-WqdKgG5KIj7qOC-q3rWgmgUrn0ISvciG5zkN-1usbp9OYvpbE&usqp=CAU);
          background-repeat: no-repeat;
          background-size: contain;
          height: 350px;
          width: 350px;
          text-align: center;
      }
  </style>
  <div class="contenedor">
      <h1>
          hola
      </h1>
  </div>  
    */
    // https://www.youtube.com/watch?v=HVl4xvsxD3g

    /*


    */

    const [prod, setProd] = useState([]);//Saqueme la lista de elementos [] y guardemela en prod

    const [search, setSearch ] = useState("");

    const searchEvent = async (e)=>{

        e.preventDefault();

        const response = await APIInvoke.invokeGET(`/producto/list/`+search);
        
        //alert(JSON.stringify(response.message))
        
        setProd(response.message)
        
        //console.log(response)

    };

    const onChange= (event) => {
        setSearch(event.target.value);   
    }


    return (

        <div>

            <div align="center">{/* Centrar Imagen y poner imagen */}
                {/* <img src="/img/logo.png" */}
                {/* <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg" */}
                <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                    height={300} />
            </div>
            {/* Nombre*/}
            <div className="text-center">
                <div className="h1"><b>CatDog</b>Store</div>
                <br />

            {/* Barra Busqueda*/}
            <div className="row justify-content-center">
                    <div className="col-12 col-md-10 col-lg-8">
                        <form className="card card-sm" onSubmit={searchEvent}>
                            <div className="card-body row no-gutters align-items-center">
                                {/*end of col*/}
                                <div className="col">
                                    <input className="form-control form-control-lg form-control-borderless" 
                                    type="text" 
                                    id="search"
                                    name="search"
                                    value={search}
                                    onChange = {onChange}
                                    placeholder="Dejanos Ayudarte...¿Que Buscas?" />
                                </div>
                                {/*end of col*/}
                                <div className="col-auto">
                                    <button className="btn btn-lg btn-success" type="submit" >Buscar</button>
                                </div>
                                {/*end of col*/}
                            </div>
                        </form>
                    </div>
                    {/*end of col*/}
                </div>
                <br />
                <br />


            {/* Barra*/}
            <div>
                <ul className="nav nav-tabs navbar-light nav-fill">
                    <li className="nav-item">
                        <Link to={"/"} className="nav-link active" href="#">Inicio</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/productos"} className="nav-link" href="#">Productos</Link>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Animal</a>
                        <div className="dropdown-menu">
                            <Link to={"/perro"} className="dropdown-item" href="#">Perro</Link>
                            <Link to={"/gato"} className="dropdown-item" href="#">Gato</Link>
                        </div>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Categorias</a>
                        <div className="dropdown-menu">
                            <Link to={"/alimentos"} className="dropdown-item" href="#">Alimentos</Link>
                            <Link to={"/limpieza"} className="dropdown-item" href="#">Limpieza</Link>
                            <Link to={"/accesorios"} className="dropdown-item" href="#">Accesorios</Link>
                        </div>
                    </li>
                    <li className="nav-item">
                        <Link to={"/login"} className="btn btn-outline-danger" href="#">Login</Link>
                    </li>
                </ul>
            </div>
            <br />

            <div className="container">
                <div className="row">
                    <div className="col-sm-8">
                        {/*jumbotron*/}
                        <div className="jumbotron jumbotron-fluid">
                            <div className="container">
                                <h1 className="display-4">Super Promos!!!</h1>
                                <p className="lead">Contamos con la mejor variedad de Productos, de excelente calidad y las mejores marcas para tu Mascota</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-4">

                        <div className="container justify-center">
                            {/*Carrusel*/}
                            <div id="carouselExampleSlidesOnly" className="carousel slide" data-ride="carousel" data-interval="2000">

                                <div className="carousel-inner">

                                    <div className="carousel-item active text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314340376_535177455282270_1072455904947998765_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFtYkG_mzaIjH0kLyznj_IXS1VBvwxP9sBLVUG_DE_2wP_klrYSjsIFQlov1_Cf1Jl8-zbG10rry_VB_qaLxEgW&_nc_ohc=ZJWLHbGTtloAX-cTfC7&_nc_ht=scontent-bog1-1.xx&oh=00_AfA6IOoaKnkIi5_tUoU4MPpePZjgGFnMRpriXbBxcbe0Xg&oe=636F1EF1" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314362031_535177551948927_6650415056530231537_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeEN0UwIq_evMFCoGqosCvzXpe-IqvtRbIKl74iq-1FsgpjM3ClwIRXmRFE-ujpcTqg8pqSJxa2pvCddpBSyYQyn&_nc_ohc=heFSV-m3RjAAX8RG7ym&_nc_ht=scontent-bog1-1.xx&oh=00_AfA7lV8q2-qHGLNMQyBwYTC2lur3IY1aab3i2wODwb-m4Q&oe=636E96D0" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314332107_535177548615594_6064375583827902353_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeH9Rt7tjYupTnnpYK5FHYUToiWvef0IOgiiJa95_Qg6COI3oRZrAuhKko3zM_B_4iqFRbehhAOmRAecg7SD-Sb6&_nc_ohc=XybP6GuzcLYAX9Wi10s&tn=5QIaVKC3aucxIfq5&_nc_ht=scontent-bog1-1.xx&oh=00_AfCkL2WJ_tGx211m844GpRGKdKTw9ewFa5eL80x0Uxzpkw&oe=636EB1E4" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314369461_535177388615610_6496439941575150178_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHIW5pRTWCibZdY7uFn4vohZ4V85sJ9LzFnhXzmwn0vMS0wnrduG0ojWmRISGp8Uhqpd25DAq0xvcx1XtgNkqnN&_nc_ohc=xlMJqwB5ihcAX_DfrRG&tn=5QIaVKC3aucxIfq5&_nc_ht=scontent-bog1-1.xx&oh=00_AfBn6IuozRwEtC9qoF3mbU32IxaHn-qAualQvF4BvcJi1w&oe=636D5BAD" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314332065_535177368615612_102048925589740457_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeEXYDSlfTvzdv_Mv2lcL6ytVS3TUCup9clVLdNQK6n1yVF8cyYiETPbKsNI0GyxJyEWveUxA2xy4blfI4LnpVu1&_nc_ohc=_2O8xRqpmLIAX8f__zM&_nc_ht=scontent-bog1-1.xx&oh=00_AfDOsD8VkNviCv3ucX-1MDztCWZf5AaAnzBIRU4pjTrdpg&oe=636D74B2" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314405260_535177301948952_8567809614806593329_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeGQGEZaDCq0W0dKn6ERNgy8r3HWH-fmlkevcdYf5-aWR8x4Ral4ayajugQ256giVQFRPqE4du75MbDDoIayNb0N&_nc_ohc=lQ7GKzndMR8AX-231-z&tn=5QIaVKC3aucxIfq5&_nc_ht=scontent-bog1-1.xx&oh=00_AfAyDjVGmtgdindh7jrxykpajwgbKgigF8WnalY0hGHAvg&oe=636E9A0B" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314371458_535177315282284_2105421757602232715_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeH5N7z_Rd0ijGhy6daSba5_m6Y3kRtuLhObpjeRG24uE_bSPkITBgE7uYWyHG48JPTxtIGNct8iz78xqZxZNf9b&_nc_ohc=FkXnYwEWAc4AX_hICTy&_nc_ht=scontent-bog1-1.xx&oh=00_AfDb_vAPhh2-XB2SuU3kdJVmGxzJodtdVjq_svYUZkhRlA&oe=636E5A61" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314418943_535177281948954_1204184725298967365_n.jpg?_nc_cat=101&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFo0xuVRe-Rw7ruplGehH4KX2rZyJcWy_RfatnIlxbL9KqPgKxBwxnNb3-H8YNXgCipsXdxEr2pUyUw8XDSMu6s&_nc_ohc=bRKe9B0XLwAAX_akLpo&tn=5QIaVKC3aucxIfq5&_nc_ht=scontent-bog1-1.xx&oh=00_AfCcCdcjs51fyUzVK-QUqI_T5If7ZLlz2vZJcS-11Tgufw&oe=636DBC1F" height={280} />
                                    </div>

                                    <div className="carousel-item text-center">
                                        <img src="https://scontent-bog1-1.xx.fbcdn.net/v/t39.30808-6/314404621_535177251948957_7650205778803223681_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHbL-1XjtUTuTK1ZSaIuDjKDiFTRaUX8kwOIVNFpRfyTImgmSz-M4LiXrf9Ah8tZPovw9s-Dg_XoDzayge_Hbht&_nc_ohc=xmF2vD_Rp40AX94j-Qp&tn=5QIaVKC3aucxIfq5&_nc_ht=scontent-bog1-1.xx&oh=00_AfAhD6H2tHPyfYPObDkPGoCnhZo0lgrGjO9nxlvYOkVELg&oe=636DE245" height={280} />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            {/*Cards*/}
            <div className="container">
                <div className="card-columns">
                    {
                        prod.map(
                            item =>
                                <div className="card border-primary" key={item._id}>
                                    <div className="card border-primary">
                                        <img src={item.urlImage} className="card-img-top" alt="..." />
                                        <div className="card-body">
                                            <p className="card-text">{item.descripcion}</p>
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item">{item.categoria}</li>
                                                <li className="list-group-item">{item.animal}</li>
                                                <li className="list-group-item h3">{"$ " + item.precio}</li>
                                            </ul>
                                            <Link to={`/agregPedido/${item._id}`} href="#" className="btn btn-primary">Comprar</Link>
                                        </div>
                                    </div>
                                </div>
                        )}
                </div>
            </div>



            <div className="container">
                {/* Texto sobre Imagen*/}
                <div className="contenedor">
                    <img src="https://st2.depositphotos.com/1003366/7225/i/450/depositphotos_72257703-stock-photo-group-of-dogs-and-kittens.jpg" height={870} />
                    <div className="centrado">
                        <h2>¿Que es CatDog Store?</h2>
                        <p className="text-center">Somos una plataforma de ventas Mayorista de productos de Perro y Gato, de las mejores marcas y al mejor precio. Contribuimos a crear un mundo mejor, ayudando a amar y proteger a sus mascotas. Invirtiendo e innovando en proyectos para ampliar la estructura y seguir creciendo en otras ciudades. Aportando y participando solidariamente para crear una comunidad inclusiva y el cuidado del medio ambiente.
                        </p>
                        <p className="text-center">Únete ahora!
                        </p></div>
                </div>
                <style dangerouslySetInnerHTML={{
                    __html:
                        "\n.contenedor{\n    position: relative;\n    display: inline-block;\n    text-align: center;\n}\n \n\n.centrado{\n    position: absolute;\n    top: 34.5%;\n    left: 54.7%;\n    transform: translate(-50%, -50%);\n}\n"
                }} />
            </div>
            <br />
            <br />
            <div className="container">
                {/* Nuestra Familia*/}
                <div className="row">
                </div>
                <div className="divider" />
                <div className="row mt-3">
                    <h2>Nuestra Familia</h2>
                </div>
                <div className="row mt-3">
                    <div className="col text-center">
                        <img src="https://http2.mlstatic.com/D_NQ_NP_775757-MLM48356530897_112021-W.jpg" height={200} />
                        <h2>Damon</h2>
                        <p>Es un Guapo y Atletico Lobo Siberiano que ama nuestros Collares</p>
                    </div>
                    <div className="col text-center">
                        <img src="https://static.vecteezy.com/system/resources/previews/004/839/206/non_2x/face-of-cat-animal-in-frame-circular-free-vector.jpg" height={200} />
                        <h2>Pacho</h2>
                        <p>Es un suave y pachoncito Gatito que adora nuestro Juguetes</p>
                    </div>
                    <div className="col text-center">
                        <img src="https://www.happets.com/static/comida-americana-pit-bull-terrier-circle-img-02-d44c057d2466e4daa8fd2eacb12e44dc.jpg" height={200} />
                        <h2>Killer</h2>
                        <p>Es un Fortachon y Jugueton PitBull que Ama nuestra Comida</p>
                    </div>
                    <div className="col text-center">
                        <img src="https://http2.mlstatic.com/D_NQ_NP_743604-MLM46636467104_072021-W.jpg" height={200} />
                        <h2>Marie, Toulouse y Berlioz</h2>
                        <p>Estos Adorables trillizos Aman el Jazz y nuestras Galletas</p>
                    </div>
                </div>
            </div>
        </div>
        </div>



    );
}

export default Home;