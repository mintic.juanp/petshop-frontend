import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import { useNavigate } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from 'sweetalert';
/*USUARIO PRUEBA    
mintic
mintic@gmail.com
1234
*/
const ProdAdm = () => {

    const [prod, setProd] = useState([]);//Saqueme la lista de elementos [] y guardemela en prod

    const loadProd = async () => {

        const response = await APIInvoke.invokeGET('/producto/list');//guarde la respuesta de esperar el APIInvoke.invokeGET('')

        setProd(response.result)
        console.log(response)
    };

    const EliminarProd = async (e, id) => {

        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/producto/${id}`);

        if (response.message == "Producto Eliminado") {

            swal({
                title: 'Eliminar Producto',
                icon: 'error',
                text: '¿Esta Seguro de Eliminar este Producto?',
                buttons: {
                    confirm: {
                        text: 'Eliminar',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    },
                    cancel: {
                        text: 'Cancelar',
                        value: false,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            }).then(async (value) => {

                if (value) {
                    e.preventDefault();
                    const response = await APIInvoke.invokeDELETE(`/producto/${id}`);

                    swal({
                        title: 'Producto Eliminado',
                        icon: 'success',
                        text: 'El Producto fue elimiando Exitosamente',
                        buttons: {
                            confirm: {
                                text: 'Ok',
                                value: true,
                                visible: true,
                                className: 'btn btn-success',
                                closeModal: true
                            }
                        }
                    });
                    loadProd();
                }
            });
        }

    }

    useEffect(() => {
        loadProd();
    }, []) //45.57

    return (
        <div>
            <div>
                {/* imagen */}
                <div align="center">{/* Centrar Imagen y poner imagen */}
                    <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                        height={300} />
                </div>
                {/* Nombre*/}
                <div className="text-center">
                    <div className="h1"><b>CatDog</b>Store</div>
                    <br />
                    <br />
                </div>
                <br />
                <br />
                {/* Barra*/}
                <div>
                    <ul className="nav nav-tabs nav-fill">
                        <li className="nav-item">
                            <Link to={'/profile'} className="nav-link " href="#">Pedidos</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/prodAdm'} className="nav-link active" href="#">Productos</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/"} className="btn btn-outline-danger " href="#">Salir</Link>
                        </li>
                    </ul>
                </div>

                <br />
                <br />

                {/*Tabla*/}

                <section className="container">
                    <div className="row align-items-center mb-4">
                        <Link to={'/agregProd'} className="btn btn-success" href="#" >Agregar Productos</Link>
                    </div>
                    <div className="divider" />
                    <table className="table table-striped table-bordered mt-4">
                        <thead>
                            <tr className="table-primary">
                                <th scope="col">ID Producto</th>
                                <th scope="col">Nombre Producto</th>
                                <th scope="col">Categoria</th>
                                <th scope="col">Animal</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                prod.map(

                                    item =>
                                        <tr key={item._id}>
                                            <td>{item._id}</td>
                                            <td>{item.nombreProducto}</td>
                                            <td>{item.categoria}</td>
                                            <td>{item.animal}</td>
                                            <td>{item.descripcion}</td>
                                            <td>{"$ "+item.precio}</td>
                                            <th>
                                                <Link type="button" to={`/editProd/${item._id}`} className="btn btn-warning btn-sm" title="Editar Producto">
                                                    <i className="fas fa-pencil-alt"></i>
                                                </Link>
                                                &nbsp;
                                                <button type="button" className="btn btn-danger btn-sm"
                                                    onClick={(e) => { EliminarProd(e,item._id) }} 
                                                    title="Eliminar Producto">
                                                    <i className="fas fa-trash"></i>
                                                </button>
                                            </th>
                                        </tr>
                                        
                                )};
                                
                        </tbody>
                    </table>
                </section>

            </div>
        </div>


    );
}

export default ProdAdm;