import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import { useNavigate } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";
import swal from 'sweetalert';
/*USUARIO PRUEBA    
mintic
mintic@gmail.com
1234
*/
const Profile = () => {

        const [ped, setPed] = useState([]);//Saqueme la lista de elementos [] y guardemela en prod

        const loadPed = async () => {
    
            const response = await APIInvoke.invokeGET('/pedidos/list');//guarde la respuesta de esperar el APIInvoke.invokeGET('')
    
            setPed(response.message)
            console.log(response)
        };
    
        const EliminarPed = async (e, id) => {
            //alert(id)
            e.preventDefault();
            const response = await APIInvoke.invokeDELETE(`/pedidos/${id}`);
    
            if (response.message =="Pedido Eliminado") {
    
                swal({
                    title: 'Eliminar Pedido',
                    icon: 'error',
                    text: '¿Esta Seguro de Eliminar este Pedido?',
                    buttons: {
                        confirm: {
                            text: 'Eliminar',
                            value: true,
                            visible: true,
                            className: 'btn btn-danger',
                            closeModal: true
                        },
                        cancel: {
                            text: 'Cancelar',
                            value: false,
                            visible: true,
                            className: 'btn btn-primary',
                            closeModal: true
                        }
                    }
                }).then(async (value) => {
    
                    if (value) {
                        e.preventDefault();
                        const response = await APIInvoke.invokeDELETE(`/pedidos/${id}`);
    
                        swal({
                            title: 'Pedido Eliminado',
                            icon: 'success',
                            text: 'El Pedido fue elimiando Exitosamente',
                            buttons: {
                                confirm: {
                                    text: 'Ok',
                                    value: true,
                                    visible: true,
                                    className: 'btn btn-success',
                                    closeModal: true
                                }
                            }
                        });
                        loadPed();
                    }
                });
            }
    
        }

        useEffect(() => {
    
            loadPed();
    
        }, []) //45.57

    return (

        <div>
            <div>
                {/* imagen */}
                <div align="center">{/* Centrar Imagen y poner imagen */}
                    <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                        height={300} />
                </div>
                {/* Nombre*/}
                <div className="text-center">
                    <div className="h1"><b>CatDog</b>Store</div>
                    <br />
                    <br />
                </div>
                <br />
                <br />
                {/* Barra*/}
                <div>
                    <ul className="nav nav-tabs navbar-light nav-fill">
                        <li className="nav-item">
                            <Link to={'/profile'} className="nav-link active" href="#">Pedidos</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/prodAdm'} className="nav-link " href="#">Productos</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/"} className="btn btn-outline-danger " href="#">Salir</Link>
                        </li>
                    </ul>
                </div>

                <br />
                <br />

                {/*Tabla*/}

                    <div className="divider" />
                    <table className="table table-striped table-bordered mt-4">
                        <thead>
                            <tr className="table-primary">
                                <th scope="col">ID Pedido</th>
                                <th scope="col">Producto</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Total Pagar</th>
                                <th scope="col">Cliente</th>
                                <th scope="col">Cedula/NIT</th>
                                <th scope="col">Direccion</th>
                                <th scope="col">Ciudad</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Email</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            ped.map(
                                    
                                item =>
                                <tr key={item._id}>
                                    <td>{item._id}</td>
                                    <td>{item.nombreProducto}</td>
                                    <td>{item.precioProducto}</td>
                                    <td>{item.cantidadProducto}</td>
                                    <td>{item.precioProducto*item.cantidadProducto}</td>
                                    <td>{item.nombreCliente}</td>
                                    <td>{item.cedula}</td>
                                    <td>{item.direccion}</td>
                                    <td>{item.ciudad}</td>
                                    <td>{item.telefono}</td>
                                    <td>{item.email}</td>
                                    <th> 
                                        &nbsp;
                                        <button type="button" className="btn btn-success btn-sm"
                                                onClick={(e) => { EliminarPed(e,item._id) }} title="Pedido Entregado">
                                                <i>Entregado</i>
                                        </button>
                                    </th>
                                </tr>
                        )};
                        </tbody>
                    </table>
                

            </div>
        </div>


    );
}

export default Profile;
