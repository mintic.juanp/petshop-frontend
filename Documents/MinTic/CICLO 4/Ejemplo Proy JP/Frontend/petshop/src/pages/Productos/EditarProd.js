import React, { useEffect, useState } from "react";//importe la extencion React
import { Link, useNavigate, useParams } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import APIInvoke from "../../utils/APIInvoke";
import swal from 'sweetalert';

//Para crear la funcion flecha rapido ponemos "sfc" y enter
const EditarProd = () => {

    const navigate = useNavigate();//es Esencial declarar estos metodos dentro del componenete principal y no en el que se usa

    const { id } = useParams();
    

    const[ Prod, setProd] = useState (
        
        
        {
            nombreProducto: '',
            urlImage:'',
            categoria:'',
            animal:'',
            descripcion:'',
            precio:''
        }
    );

    const{nombreProducto,urlImage,categoria,animal,descripcion,precio} = Prod;

    const onChange= (event) => {

        setProd(
            {
                ...Prod,// el ...Prod se usa para pedir que pase el objeto desglosado elemento por elemento
                [event.target.name]:event.target.value//creeme un elemento llave:valor con el nombre del elemeneto que genero el evento y el valor digitado
            }
        );
    }

    const onSubmit = (event) => {//funcion para evitar que envie los datos del registro por la url, pues por defecto si no se pone un metodo el navegador asueme que es un GET
        event.preventDefault();//prevenga el evento, que se envien los datos por los datos del registro por la url
        saveProd();
    }

    const saveProd=async()=>{
        
        const data={
        
                nombreProducto: Prod.nombreProducto,
                urlImage:Prod.urlImage,
                categoria:Prod.categoria,
                animal:Prod.animal,
                descripcion:Prod.descripcion,
                precio:Prod.precio
            
        }

        const response = await APIInvoke.invokePUT(`/producto/${id}`, data)

        if(response.message==="Producto actualizado"){

            swal({
                title: 'Producto Actualizado',
                icon: 'success',
                text:'Producto Actualizado Exitosamente',
                buttons:{
                    confirm:{
                        text:'Ok',
                        value: true,
                        visible: true,
                        className:'btn btn-success',
                        closeModal: true
                    }
                }
            }).then(  //.then = entonces
                navigate('/prodAdm')
            )

        }else{

            swal({
                title: 'Error Desconocido',
                icon: 'error',
                text:'NO se pudo Actualizar Producto, Pongase en contacto con el SuperAdministrador',
                buttons:{
                    confirm:{
                        text:'Ok',
                        value: true,
                        visible: true,
                        className:'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }
    }

    useEffect(()=>{//funcion para que se ponga el cursor automaticamenete en email
        
        async function loadProd(){

            const response = await APIInvoke.invokeGET(`/producto/${id}`);//Para Cargar los Datos en el Formulario

            setProd (response.message);

            console.log(response)

            return;
        }

        loadProd();
        document.getElementById("nombreProducto").focus();

    },[]); 

    return (

        <div className="jumbotron">

            <div align="center">{/* Centrar Imagen y poner imagen */}
                <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                    height={100} />
            </div>
            {/* Nombre*/}
            <div className="text-center">
                <div className="h1"><b>CatDog</b>Store</div>
                <br />
            </div>

            <form onSubmit={onSubmit}>  {/* Cuando enel formulario haya un onSubmit(Boton crear Producto), llame a la funcion onSubmit*/}
                <h2>Editar los Datos de un Producto:</h2>
                <br />
                <br />
                <div className="form-row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault01">Nombre Producto</label>
                        <input type="text" className="form-control" 
                        id="nombreProducto"
                        name="nombreProducto"
                        value={nombreProducto}//el avlor de este campo es la nombreProducto sacado del setUser
                        onChange={ onChange }//cuando cambie llame al metodo onChange
                        placeholder="Nombre Producto ..."
                        required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">URL Imagen</label>
                        <input type="text" className="form-control" 
                        id="urlImage"
                        name="urlImage"
                        value={urlImage}//el avlor de este campo es la URL Imagen sacado del setUser
                        onChange={ onChange }//cuando cambie llame al metodo onChange
                        placeholder="URL Imagen ..." 
                        required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault04">Categoria</label>
                        <select id="categoria" name="categoria" 
                        className="custom-select"
                        value={categoria}//el avlor de este campo es la Categoria sacado del setUser
                        onChange={ onChange }//cuando cambie llame al metodo onChange
                        required>
                            <option value={""} selected disabled>Seleccione...</option>
                            <option value={"Comida"}>Alimentos</option>
                            <option value={"Limpieza"}>Limpieza</option>
                            <option value={"Accesorios"}>Accesorios</option>
                        </select>
                    </div>

                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault04">Animal</label>
                        <select id="animal" name="animal" 
                        className="custom-select"
                        value={animal}//el avlor de este campo es la animal sacado del setUser
                        onChange={ onChange }//cuando cambie llame al metodo onChange
                        required>
                            <option value={""} selected disabled>Seleccione...</option>
                            <option value={"Perro"}>Perro</option>
                            <option value={"Gato"}>Gato</option>
                        </select>
                    </div>

                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Descripcion</label>
                        <input type="text" className="form-control" 
                        id="descripcion"
                        name="descripcion"
                        value={descripcion}//el avlor de este campo es la Descripcion sacado del setUser
                        onChange={ onChange }//cuando cambie llame al metodo onChange
                        placeholder="Peso/Tamaño/Color/Gramos...etc" 
                        required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Precio</label>
                        <input type="text" className="form-control" 
                        id="precio"
                        name="precio"
                        value={precio}//el avlor de este campo es la precio sacado del setUser
                        onChange={ onChange }//cuando cambie llame al metodo onChange 
                        placeholder="Precio con IVA..." 
                        required />
                    </div>
                    <br />

                    <br />
                    <h6>NOTA: Tenga en Cuenta que esta Informacion se ingresa al Sistema pero el SuperAdministrador debe aprobarla para que aparezca en la Pagina</h6>
                    <br />
                </div>
                <br />
                <button type="submit" class="btn btn-primary btn-lg">Guardar</button>
                &nbsp;
                <Link to={'/'} type="button" className="btn btn-danger btn-lg">Volver</Link>
            </form>

        </div>


    );
}

export default EditarProd;