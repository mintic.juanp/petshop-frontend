import React, { useEffect, useState } from "react";//importe la extencion React
import { Link, useNavigate, useParams } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import APIInvoke from "../../utils/APIInvoke";
import swal from 'sweetalert';

//Para crear la funcion flecha rapido ponemos "sfc" y enter


const AgregarPedido = () => {

    const navigate = useNavigate();

    const [pedido, setPedido] = useState(

        {
            cantidadProducto: '',
            nombreCliente: '',
            cedula: '',
            ciudad: '',
            direccion: '',
            telefono: '',
            email: ''
        }
    );

    var { cantidadProducto, nombreCliente, cedula, ciudad, direccion, telefono, email } = pedido;

    const [prod, setProducto] = useState(

        {
            nombreProducto: '',
            urlImage: '',
            categoria: '',
            animal: '',
            descripcion: '',
            precio: ''
        }
    );

    var { nombreProducto, urlImage, categoria, animal, descripcion, precio } = prod;


    const onChange = (event) => {

        setPedido(
            {
                ...pedido,// el ...Prod se usa para pedir que pase el objeto desglosado elemento por elemento
                [event.target.name]: event.target.value//creeme un elemento llave:valor con el nombre del elemeneto que genero el evento y el valor digitado
            }
        );
    }

    const onSubmit = (event) => {//funcion para evitar que envie los datos del registro por la url, pues por defecto si no se pone un metodo el navegador asueme que es un GET
        event.preventDefault();//prevenga el evento, que se envien los datos por los datos del registro por la url
        savePed();
    }

    const savePed = async () => {
    //alert(JSON.stringify(prod))
    //alert(JSON.stringify(pedido))
        const data = {

            nombreProducto: prod.nombreProducto,
            precioProducto: prod.precio,
            cantidadProducto: pedido.cantidadProducto,
            nombreCliente: pedido.nombreCliente,
            cedula: pedido.cedula,
            ciudad: pedido.ciudad,
            direccion: pedido.direccion,
            telefono: pedido.telefono,
            email: pedido.email

        }
        
        //alert(JSON.stringify(data))

        const response = await APIInvoke.invokePOST('/pedidos/save', data)

        if(response.message==="Pedido Agregado"){

            swal({
                title: 'Pedido Agregado',
                icon: 'success',
                text:'Nuestro Equipo se pondra en contacto contigo.',
                buttons:{
                    confirm:{
                        text:'Ok',
                        value: true,
                        visible: true,
                        className:'btn btn-success',
                        closeModal: true
                    }
                }
            }).then(  //.then = entonces

                navigate('/')
            )

        }else{

            console.log(response.error)
            console.log(response)
            swal({
                title: 'Error Desconocido',
                icon: 'error',
                text:'NO se pudo Agregar Pedido, Intentelo nuevamente o Comuniquese al 018000 725 49 86',
                buttons:{
                    confirm:{
                        text:'Ok',
                        value: true,
                        visible: true,
                        className:'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }


    }


    //------------------------------------------

    const { id } = useParams();


    useEffect(() => {//funcion para que se ponga el cursor automaticamenete en email

        async function loadProd() {

            const response = await APIInvoke.invokeGET(`/producto/${id}`);

            setProducto(response.message);

            return;
        }

        loadProd();
        document.getElementById("nombreCliente").focus();

    }, []);


    return (

        <div className="jumbotron" >

            <div align="center">{/* Centrar Imagen y poner imagen */}
                <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                    height={100} />
            </div>
            {/* Nombre*/}
            <div className="text-center">
                <div className="h1"><b>CatDog</b>Store</div>
                <br />
            </div>
            <br />
                <br />
            <h2>Registra tus Datos para hacer tu Comprar...</h2>
                <br />


            <div className="card mb-3" style={{ maxWidth: 1000 }}>
                <div className="row no-gutters">
                    <div className="col-md-4">
                        <img  className="img-fluid" src={urlImage}  />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h2 className="card-text">Caracteristicas Producto</h2>
                            <br />
                            <h5 className="card-text">{" + "+descripcion}</h5>
                            <h5 className="card-text">{" + "+categoria}</h5>
                            <h5 className="card-text">{" + "+animal}</h5>
                            <br />
                            <h4 className="card-text">{"$ " + precio}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <form>
                

                <div className="form-row">

                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault01">Nombre Completo</label>
                        <input type="text" className="form-control"
                            id="nombreCliente"
                            name="nombreCliente"
                            value={nombreCliente}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..."
                            placeholder="Tu Nombre Completo..."
                            required />
                    </div>


                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Cedula</label>
                        <input type="text" className="form-control"
                            id="cedula"
                            name="cedula"
                            value={cedula}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..."
                            placeholder="Tu Numero de ID..."
                            required />
                    </div>

                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Telefono</label>
                        <input type="text" className="form-control"
                            id="telefono"
                            name="telefono"
                            value={telefono}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..." 
                            placeholder="Tu Telefono..."
                            required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Email</label>
                        <input type="text" className="form-control"
                            id="email"
                            name="email"
                            value={email}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..."
                            placeholder="Tu Email..."
                            required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Ciudad</label>
                        <input type="text" className="form-control"
                            id="ciudad"
                            name="ciudad"
                            value={ciudad}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..." 
                            placeholder="Ciudad donde Resides..."
                            required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Direccion</label>
                        <input type="text" className="form-control"
                            id="direccion"
                            name="direccion"
                            value={direccion}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..." 
                            placeholder="Direccion donde Resides..."
                            required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Cantidad</label>
                        <input type="text" className="form-control"
                            id="cantidadProducto"
                            name="cantidadProducto"
                            value={cantidadProducto}//el avlor de este campo es la nombreProducto sacado del setUser
                            onChange={onChange}//cuando cambie llame al metodo onChange placeholder="Tu Nombre Completo..." 
                            placeholder="Direccion donde Resides..."
                            required />
                    </div>

                </div>
            </form>


            
            <br />
            <br />

            <div className="list-group">
                <a href="#" className="list-group-item list-group-item-action list-group-item-danger">NOTA: Recuerda que somos una tienda mayorista, por lo que hacemos un pedido por cada producto, esto para pedirlo directamente en Fabrica o Embarque.</a>
                <a href="#" className="list-group-item list-group-item-action list-group-item-warning">     + Nuestro Equipo se pondra en contacto contigo.</a>
                <a href="#" className="list-group-item list-group-item-action list-group-item-info">       + Te Enviaremos la factura al E-Mail.</a>
                <a href="#" className="list-group-item list-group-item-action list-group-item-info">       + Recuerda que puedes Pagar Contra-Entrega o al Numero de Cuenta 784521885-9 (Banco XXX).</a>
                <a href="#" className="list-group-item list-group-item-action list-group-item-info">       + Tienes 30 Dias para devolver el producto.</a>
            </div>

            <br />

            <button type="submit" class="btn btn-primary btn-lg"
            onClick={onSubmit} >Registrar Compra</button>
            &nbsp;
            <Link to={'/productos'} type="button" className="btn btn-danger btn-lg">Volver</Link>


        </div>

    );
}

export default AgregarPedido;

/*

<div className="col">
                        <div className="card" style={{ width: '18rem' }}>
                            <img src="..." className="card-img-top" alt="..." />
                            <div className="card-body">
                                <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>

                    <div className="col">
                        <ul className="list-group">
                            <li className="list-group-item active" aria-current="true">An active item</li>
                            <li className="list-group-item">A second item</li>
                            <li className="list-group-item">A third item</li>
                            <li className="list-group-item">A fourth item</li>
                            <li className="list-group-item">And a fifth one</li>
                        </ul>
                    </div>

*/
/*

<div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src={urlImage} alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Caracteristicas Producto</h5>
                            <p class="card-text">{descripcion}</p>
                            <p class="card-text">{categoria}</p>
                            <p class="card-text">{animal}</p>
                            <p class="card-text">{"$ " + precio}</p>
                        </div>
                    </div>
                </div>
            </div>
*/