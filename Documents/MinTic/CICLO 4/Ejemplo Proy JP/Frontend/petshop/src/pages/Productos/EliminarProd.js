import React, { useEffect, useState } from "react";//importe la extencion React
import { Link } from "react-router-dom";//Importe el componenete link de la extension "react-router-dom"
import APIInvoke from "../../utils/APIInvoke";

//Para crear la funcion flecha rapido ponemos "sfc" y enter
const ElimProd = () => {


    return (

        <div className="jumbotron">

            <div align="center">{/* Centrar Imagen y poner imagen */}
                        <img src="https://static.vecteezy.com/system/resources/previews/006/470/731/non_2x/modern-animal-pet-logo-template-free-vector.jpg"
                            height={100} />
                    </div>
                    {/* Nombre*/}
                    <div className="text-center">
                        <div className="h1"><b>CatDog</b>Store</div>
                        <br />
                        </div>
                        
            <form>
                <h2>Registre los Datos del nuevo Producto a ofrecer en la CatDogStore:</h2>
                <br />
                <br />
                <div className="form-row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault01">Nombre Producto</label>
                        <input type="text" className="form-control" id="validationDefault01" placeholder="Nombre Producto ..." required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">URL Imagen</label>
                        <input type="text" className="form-control" id="validationDefault02" placeholder="URL Imagen ..." required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Categoria</label>
                        <input type="text" className="form-control" id="validationDefault02" placeholder="Alimentos, Limpieza o Accesorios" required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Animal</label>
                        <input type="text" className="form-control" id="validationDefault02" placeholder="Perro o Gato" required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Descripcion</label>
                        <input type="text" className="form-control" id="validationDefault02" placeholder="Peso/Tamaño/Color/Gramos...etc" required />
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="validationDefault02">Precio</label>
                        <input type="text" className="form-control" id="validationDefault02" placeholder="Precio con IVA..." required />
                    </div>
                    <br />
                    <br />
                    <h6>NOTA: Tenga en Cuenta que esta Informacion se ingresa al Sistema pero el SuperAdministrador debe aprobarla para que aparezca en la Pagina</h6>
                    <br />
                </div>
                <br />
                <button type="button" class="btn btn-primary btn-lg">Registra Compra</button>
                &nbsp;
                <Link to={'/prodAdm'} type="button" className="btn btn-danger btn-lg">Volver</Link>
                </form>

        </div>


    );
}

export default ElimProd;