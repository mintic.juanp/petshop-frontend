import config from '../config'
//Este archivo lo paso el profe y sirve para invocar con GET,POST,PUT Y DELETE
class APIInvoke {

    async invokeGET(resource, queryParams) {

        queryParams = queryParams || []
        const queryString = queryParams.reduce((last, q, i) => last + `${i === 0 ? '?' : "&"}${q}`, '')

        const token = localStorage.getItem("token");
        let bearer;
        if (token === "") {
            bearer = "";
        } else {
            bearer = `Bearer ${token}`;
        }

        const data = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearer
            }
        }
        const url = `${config.api.baseURL}${resource}${queryString}`
        let response = (await (await fetch(url, data)).json())
        return response
    }

    async invokePUT(resource, body) {

        const token = localStorage.getItem("token");
        let bearer;
        if (token === "") {
            bearer = "";
        } else {
            bearer = `Bearer ${token}`;
        }

        const data = {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearer
            }
        }
        const url = `${config.api.baseURL}${resource}`
        let response = (await (await fetch(url, data)).json())
        return response
    }

    async invokePOST(resource, body) {

        //Para verficar el token
        const token = localStorage.getItem("token");
        
        let bearer;
        
        if (token === "") {
            bearer = "";
        } else {
            bearer = `Bearer ${token}`;
        }
        //Para aramar el cuerpo de la solicitud
        const data = {
            method: 'POST',
            body: JSON.stringify( body ),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearer
            }
        }
        //Arme el enlace de consulta 
        const url = `${config.api.baseURL}${resource}`;//Arme el enlace de consulta URL ejem: http://localhost:5000/producto/save
        let response = (await (await fetch(url, data)).json());//cargue url(fetch) y mande data, espere la Respuesta(await) y trasformelo en json()
        return response;//retorne la respuesta final
    }

    async invokeDELETE(resource) {

        const token = localStorage.getItem("token");
        let bearer;
        if (token === "") {
            bearer = "";
        } else {
            bearer = `Bearer ${token}`;
        }

        const data = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': bearer
            }
        }
        const url = `${config.api.baseURL}${resource}`
        let response = (await (await fetch(url, data)).json())
        return response
    }
}

export default new APIInvoke();